# [DRF Skeleton][] - CHANGELOG

All notable changes to this project will be documented in this file.

---

## [Unreleased][] - 2017-04-26
### Added
-   bin/wait-for-it.sh to control the boot order of the Docker containers
-   Requirements, Built With and Acknowledgments sections to the [README][]

---

## [Unreleased][] - 2017-04-25
### Added
-   Volume a volume to docker-compose for Postgresql
-   The Data folder to .gitignore
-   localhost and 127.0.0.1 to "ALLOWED_HOSTS" in settings.py

### Changed
-   The engine in which Django connects to django.db.backends.postgresql_psycopg2
-   The default url to be pointed to the Content view

---

## [Unreleased][] - 2017-04-21
### Added
-   Initial Commit

---

*The format is based on [Keep a CHANGELOG](http://keepachangelog.com)*

---

[DRF Skeleton]: https://gitlab.com/jhillen/docker-drf-skeleton
[README]:       https://gitlab.com/jhillen/docker-drf-skeleton/blob/master/README.md
[Unreleased]:   https://gitlab.com/jhillen/docker-drf-skeleton
