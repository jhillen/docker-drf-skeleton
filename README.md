# [DRF Skeleton][]

Powerful shell for rapidly deploying your Django Rest Framework projects.

*   Project : [https://gitlab.com/jhillen/docker-drf-skeleton](https://gitlab.com/jhillen/docker-drf-skeleton)
*   Author : [J.D. Hillen](http://jdhillen.com) // [@jdhillen](http://twitter.com/jdhillen)

---

## Requirements
*   Git
*   Docker

---

## Installation

    docker-compose up

    docker-compose run web python ./src/manage.py makemigrations

    docker-compose run web python ./src/manage.py migrate

    docker-compose run web python ./src/manage.py createsuperuser

    docker-compose down

    docker-compose up

---

## Built With
*   [Python](https://www.python.org/)
*   [Django](https://www.djangoproject.com)
*   [Django Rest Framework](http://www.django-rest-framework.org)
*   [PostgreSQL](https://www.postgresql.org)
*   [Docker](https://www.docker.com)

---

## Acknowledgments

*   [Wait-For-It][] - Their bash script allows the control of the boot order of the docker containers.

---

[DRF Skeleton]: https://gitlab.com/jhillen/docker-drf-skeleton
[Wait-For-It]: https://github.com/vishnubob/wait-for-it
